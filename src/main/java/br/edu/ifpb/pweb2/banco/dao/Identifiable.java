package br.edu.ifpb.pweb2.banco.dao;

public interface Identifiable {
	
	Integer getId();
	void setId(Integer id);

}
