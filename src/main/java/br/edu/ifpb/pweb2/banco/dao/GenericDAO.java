package br.edu.ifpb.pweb2.banco.dao;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.stereotype.Repository;

@Repository
public class GenericDAO<T extends Identifiable> implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private Set<T> objects = new HashSet<T>();
	
	public T findById(Integer id) {
		for (T a : objects) {
			if (a.getId().equals(id)) {
				return a;
			}
		}
		return null;
	}
	
	public T find(T aluno) {
		for (T a : objects) {
			if (a.equals(aluno)) {
				return a;
			}
		}
		return null;
	}
	
	public boolean delete(T aluno) {
		return objects.remove(aluno);
	}
	
	public T update(T aluno) {
		T x = this.findById(aluno.getId());
		if (x != null) {
			this.objects.remove(x);
			this.objects.add(aluno);
			return aluno;
		}
		return x;
	}
	
	public Integer insert(T aluno) {
		Integer maxId = this.findMaxId();
		aluno.setId(++maxId);
		objects.add(aluno);
		return maxId;
	}

	// Olha mãe! Sem usar for!
	private Integer findMaxId() {
		Optional<T> a =  objects.stream().max(Comparator.comparing(T::getId));
		if (a.isPresent()) {
			return a.get().getId();
		} else {
			return 0;
		}
	}

	public List<T> findAll() {
		return new ArrayList<T>(objects);
	}

}
